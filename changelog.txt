##############################################################

# Changelog
# Stig - Multipurpose One/Multi Page Template
# Full Changelog - https://themeforest.net/item/stig-multipurpose-onemulti-page-template/11239642#item-description__changelog

##############################################################

v. 1.2.4 � 29 July 2018

- Google Maps Api was replaced with Google Maps embed (details - https://forums.envato.com/t/important-changes-to-the-google-maps-platform/173357):
--- file /css/style.css was updated ("Google Maps" section)
--- file /css/style-responsive.css was updated
--- file /js/all.js was updated ("Google Maps" section)
--- all HTML files were updated (removed including of Google Maps Api and file /js/gmap3.min.js)
--- HTML code of Google Maps sections was updated
--- file /js/gmap3.min.js was removed


#